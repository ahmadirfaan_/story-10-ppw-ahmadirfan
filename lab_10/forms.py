from django.forms import ModelForm, TextInput

from .models import Registration

class RegistrationForm(ModelForm):

    class Meta:
        model = Registration
        fields = ('email', 'nama', 'password')
        widgets = {
            'email':TextInput(attrs={
                'required':'True',
                'placeholder': 'Enter your email',
                'class': 'form-control',
                'id': 'email',
                'type': 'email',
            }),
            'nama':TextInput(attrs={
                'required': 'True',
                'placeholder': 'Enter your name',
                'class': 'form-control',
                'id': 'nama',
                'type': 'nama'
            }),
            'password':TextInput(attrs={
                'required': 'True',
                'placeholder': 'Enter your password',
                'class': 'form-control',
                'id': 'password',
                'type': 'password'
            }),
        }
var bool_email = false;

$("#email").change(function () {
    var email = $(this).val();
    $.ajax({
      url: '/validate_email',
          data: {
            'email': email
          },
          dataType: 'json',
          success: function (data) {
            $('#submit_button').prop('disabled', false);
            if (data.is_created) {
              alert("A user with this email already exists.");
              $('#submit_button').prop('disabled', true);
            }
          },
          error : function(){
            alert("Fungsi ga kepanggil!");
          }
        });
  });

function daftarPost (){
  $.ajax({
    type: 'POST',
    url: '/create_registration',
    data:{
      email: $('#email').val(),
      nama: $('#nama').val(),
      password: $('#password').val(),
      csrfmiddlewaretoken : $("input[name=csrfmiddlewaretoken]").val()
    },
    success:function(){
      alert("Created new user");
    }
  })
};


function challenge(){
  $.ajax({
    type: 'GET',
    url: '/challenge',
    dataType: 'json',
    success:function(data){
  
      document.getElementById("data").innerHTML = 
      data + "<br>"
    }
  })
  
}

from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from datetime import date
import unittest
from .models import Registration
from .forms import RegistrationForm
from .views import index, challenge
from selenium import webdriver
import time
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class Lab1UnitTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome("./chromedriver",chrome_options=chrome_options)


    def test_lab_10_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_else_does_not_exist(self):
        response = Client().get('/antah-berantah/')
        self.assertEqual(response.status_code,404)

    def test_lab_10_using_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'lab10.html')

    def tearDown(self):
        self.browser.quit()

    
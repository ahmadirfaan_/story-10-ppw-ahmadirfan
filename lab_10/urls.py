from django.urls import re_path
from .views import index, validate_email, create_registration, challenge
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^validate_email', validate_email, name='validate_email'),
    re_path(r'^create_registration', create_registration, name='create_registration'),
    re_path(r'^challenge', challenge, name='challenge')
]

from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from .forms import RegistrationForm
from .models import Registration
import requests
from django.core import serializers

def index(request):
	form = RegistrationForm(request.POST)
	return render(request, "lab10.html", {'form': RegistrationForm()})


def validate_email(request):
	email = request.GET.get('email', None)
	data = {"is_created" :  Registration.objects.filter(email__iexact=email).exists()}
    
	return JsonResponse(data)

def create_registration(request):
	form = RegistrationForm(request.POST)

	if request.method == 'POST':
		if form.is_valid():
			instance = form.save()
			print("Berhasil")
		else:
			print(form.errors)

	return render(request, "lab10.html", {
		"form" : RegistrationForm,
	})

def challenge(request):
	data_dict = {}
	data = Registration.objects.all()
	data2 = serializers.serialize('json', data)
	
	return JsonResponse(data2, safe=False)